# _template

The purpose of this project is to try making a drop-in CI configs for *building docker files only*.
It requires `Dockerfile` to be present in project root, and treats it as black box. It doesn't
pre compile things, it doesn't run unit tests, it doesn't output coverage, it doesn't care about
anything other than what's inside `Dockerfile`. Use multi-stage builds if you want to extend it, or
add proper shell scripts and stages, up to you.

What it does:
- *check*: stage runs sanity checks on repo before building things. This includes self-shellchecking
  and hadolinting docker files.
- *build*: uses docker:dind as a service and docker to build image and upload it to gitlab's registry
- *test*: attempts to run the built image, maps `goss` binary into it and runs all integration tests
  from `.gitlab.d/ci/tests` directory. Fails the build if any of those fail.
- *publish*: tags the built image as `latest` and as `{{.Config.Labels.version}}`, and publishes it to
  internal registry.

### Usage (presumed)

- copy both `.gitlab-ci.yml` and `.gitlab.d` into your project
- edit `.gitlab-ci.yml` to suit your needs (remove `exit 0` to the very least)
- add tests and build stuff

### Local usage notes

In order to run build script, you need to `docker login registry.gitlab.com` first.
Use API scope token instead of password if you have 2fa set up.
If you have different registry, then please contribute documentation here.
Script doesn't try to be overly smart and just assumes you're logged in into registry.

### Template copy checklist for new docker file project
- [ ] copy `.gitlab-ci.yml` to the project root
- [ ] edit `.gitlab-ci.yml` to suit your needs:
  - [ ] remove `exit 0` plugs from test/publish stages
  - [ ] remove TRIGGER variables section or set it to proper value
- [ ] copy `.gitlab.d` to the project root
  - [ ] run `bash .gitlab.d/ci/scripts/check.sh` and make sure there's no error
- [ ] edit `README.md`
- [ ] add proper tests
